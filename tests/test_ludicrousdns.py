import os.path
import subprocess

import pytest

from ludicrousdns import ResolverPool
from ludicrousdns import filter_nameservers


def test_resolver():
    resolver = ResolverPool()
    result = resolver.resolve_hosts(['example.com', 'google.com', 'doesntexist.example.com'])
    assert set(x[0] for x in result) == {'example.com', 'google.com'}

    result = resolver.resolve_hosts(['youtube.com'])
    assert result[0][0] == 'youtube.com'


def test_wildcard_dns_detection():
    resolver = ResolverPool(detect_wildcard_dns=False)
    result = resolver.resolve_hosts(['doesntexist.github.io', 'doesntexist.docs.google.com'])
    assert set(x[0] for x in result) == {'doesntexist.github.io', 'doesntexist.docs.google.com'}

    smart_resolver = ResolverPool(detect_wildcard_dns=True)
    result = smart_resolver.resolve_hosts(
        ['asdfqwerty.github.io',
         'docs.google.com', 'doesntexist.docs.google.com'])
    assert set(x[0] for x in result) == {'docs.google.com'}


def test_filter_nameservers():
    nameservers = ["1.1.1.1", "192.0.2.0"]
    filtered = filter_nameservers(nameservers)
    assert filtered == ["1.1.1.1"]


@pytest.mark.slow
def test_accuracy():
    current_dir = os.path.dirname(os.path.realpath(__file__))
    with open(current_dir + "/subdomains-top1mil-5000.txt") as f:
        subdomains = [line.strip() for line in f]

    hosts = [sub + ".google.com" for sub in subdomains]

    resolver = ResolverPool()
    resolved_hosts = [h[0] for h in resolver.resolve_hosts(hosts)]

    script = """
while read host
do
    if [ -n "$(dig @8.8.8.8 $host +short)" ]; then
        echo "$host"
    fi
done
"""
    p = subprocess.run(script,
                       stdout=subprocess.PIPE,
                       input=b'\n'.join(host.encode('utf-8') for host in hosts),
                       shell=True, check=True)
    accurately_resolved_hosts = [host for host in p.stdout.decode('utf-8').split('\n') if host]

    print("{} == {}".format(len(set(resolved_hosts)), len(set(accurately_resolved_hosts))))
    assert set(resolved_hosts) == set(accurately_resolved_hosts)
